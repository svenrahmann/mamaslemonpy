Index large biological sequences (DNA, RNA) with enhanced suffix arrays and run maximal matches queries and approximate backward search on them.

