import random
import mamaslemonpy
from mamaslemonpy.suffix import SuffixArray

_CHARS = "$ACGT."
_DNA = (1,2,3,4)*95 + (5,5,5,5)*5
_OPT = (False,)*90 + (True,)*10

def patternstring(pat):
    return "".join(_CHARS[x] for x in pat)

def random_pattern(length, optionals=False):
    p = [random.choice(_DNA) for i in range(length)]
    if not optionals:
        return p
    o = [random.choice(_OPT) for i in range(length)]
    return p, o

def edit_distance_at(P, seq, j):
    m = len(P); n = len(seq)
    jj = 0
    endj = j + 2*m
    if endj > n: endj = n
    lastcol = [i for i in range(m+1)]
    thiscol = [0 for i in range(m+1)]
    ed = lastcol[m];  bestj = jj
    while j < endj:
        jj += 1
        thiscol[0] = jj
        for i in range(1, m+1):
            e = P[i-1] != seq[j]
            thiscol[i] = min(thiscol[i-1]+1, lastcol[i]+1, lastcol[i-1]+e)
        if thiscol[m] < ed:  ed = thiscol[m]; bestj = jj
        j += 1
        lastcol, thiscol = thiscol, lastcol
    return ed, bestj

def subinterval_exists(L, tup):
    (d0,ms0,L0,R0) = tup
    for (d,ms,L,R) in L:
        if d!=d0 or ms!=ms0: continue
        if L>=L0 and R<=R0: return True  # subinterval found
    return False

def overlapping_interval_exists(L, tup):
    (d0,ms0,L0,R0) = tup
    for (d,ms,L,R) in L:
        if d!=d0 or ms!=ms0: continue
        if L>=L0 and R<=R0: return True  # subinterval found
        if L0>=L and R0<=R: return True  # encolsing interval found
    return False

def verify_match_with_errors(pat, errors):
    pos = sa.pos;  seq = sa.s
    results = list(sa.match_with_errors(pat, errors))
    for (e, ms, L, R) in results:
        # pat[ms:] aligns to text at position p with e errors
        # for p = pos[r] for each r in [L,R]
        P = pat[-ms:] if ms > 0 else []
        print("  result:", (e,ms,L,R), "matching part:", patternstring(P))
        positions = [pos[r] for r in range(L,R+1)]
        for p in positions:
            d, j = edit_distance_at(P, seq, p)
            assert d <= e, "P={} vs. T={}".format(patternstring(P), patternstring(seq[p:p+j]))
            if d < e:
                assert overlapping_interval_exists(results,(d, ms, L, R)),\
                       "{} with d={} should imply {}".format((e,ms,L,R),d,(d,ms,L,R))

######################################################################

def setup():
    global sa
    indexname = "./gludna"
    sa = SuffixArray.from_indexname(indexname)
    sa.bwt_from_indexname(indexname)
    sa.occ_from_indexname(indexname)
    sa.pos_from_indexname(indexname)
    sa.seq_from_indexname(indexname)

def test_match_with_errors_case_1():
    p = "CACTTTAACGTCAAT"
    pat = [_CHARS.find(x) for x in p]
    verify_match_with_errors(pat, 0)

def test_match_with_errors_case_2():
    p = "TGGCTCAGCCTTGGC"
    pat = [_CHARS.find(x) for x in p]
    verify_match_with_errors(pat, 2)

def test_match_with_errors_case_3():
    p = "GCCATGAGAACTTTG"
    pat = [_CHARS.find(x) for x in p]
    verify_match_with_errors(pat, 2)

def test_match_with_errors_case_4():
    p = "CACCAGTGCTCTGCT"
    pat = [_CHARS.find(x) for x in p]
    verify_match_with_errors(pat, 2)

def Xtest_match_with_errors_wildcard_at_end():
    p = "CGTCTGTTCAAGAA."
    pat = [_CHARS.find(x) for x in p]
    verify_match_with_errors(pat, 1)

def Xtest_match_with_errors_single_letter():
    p = "CTCTTATCTTT.A.T"
    pat = [_CHARS.find(x) for x in p]
    verify_match_with_errors(pat, 0)

def Xtest_match_with_errors_case_endswith_TA():
    p = "GCGTCCGCAA...TA"
    pat = [_CHARS.find(x) for x in p]
    verify_match_with_errors(pat, 2)
    
def test_match_with_errors_random_patterns():
    num = 1000
    length = 15
    errors = 2
    for i in range(num):
        pat = random_pattern(length)
        print("*** Pattern:", patternstring(pat))
        verify_match_with_errors(pat, errors)


######################################################################

def notest_backward_matching_statistics_single_T():
    p = "CTCTTATCTTT.A.T"
    pat = [_CHARS.find(x) for x in p]
    for bms in sa.backward_matching_statistics(pat):
        print(bms)

def notest_backward_matching_statistics_non_existing():
    p = "CACTTTAACGTCAAT"
    pat = [_CHARS.find(x) for x in p]
    for bms in sa.backward_matching_statistics(pat):
        print(bms)

######################################################################

def main():
    setup()
    notest_backward_matching_statistics_non_existing()

if __name__ == "__main__":
    main()
